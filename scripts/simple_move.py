
from nmstagepy.nmstage import Controller, define_step_unit
import astropy.units as u

# default com for mac os x for keyspan USB to serial converter
com = '/dev/cu.KeySerial1'

nm = Controller()
nm.connect(com)

step = define_step_unit()

nm.set_axis('z')
nm.move_relative(-10 * step)