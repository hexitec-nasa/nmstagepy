"""
To run:
python -m bokeh serve <name of this file>
"""
import logging
from io import StringIO

import astropy.units as u
from bokeh.layouts import widgetbox
from bokeh.models.widgets import Button, RadioButtonGroup, PreText, TextInput, Div, Select
from bokeh.plotting import curdoc

from nmstagepy.nmstage import Controller, define_step_unit

log_stream = StringIO()
logging.basicConfig(stream=log_stream, level=logging.INFO, format='%(asctime)s %(message)s', datefmt='%Y/%m/%d %H:%M:%S')
console_text = ""

doc = curdoc()

WIDTH = 400

nm = Controller()

direction_list = ['X', 'Y', 'Z']
step = define_step_unit()


def update_console_text():
    print(log_stream.getvalue())
    pre.text = str(log_stream.getvalue())


def connect_action():
    try:
        nm.connect(com_input.value)
        connect_button.button_type = 'success'
    except IOError:
        logging.warning("Connect attempt to {0} failed".format(com_input.value))
        update_console_text()
        pass


def move_action():
    nm.set_axis(direction_list[direction_radio_button_group.active].lower())
    distance = float(distance_input.value) * u.Unit(unit_select.value)
    print(distance)

    if move_type_radio_buttom_group.active == 0:  # relative
        nm.move_relative(distance)
    if move_type_radio_buttom_group.active == 1:  # absolute
        nm.move_absolute(distance)


def stop_action():
    nm.stop()


def send_action():
    nm.send_command(raw_input.value)


title_div = Div(text="<h1>Newmark Stage Controller</h1>")
move_button = Button(label="Move", button_type="success")
move_button.on_click(move_action)
connect_button = Button(label="Connect", button_type="warning")
connect_button.on_click(connect_action)

com_input = TextInput(value='/dev/cu.KeySerial1', title="COM port")
stop_button = Button(label="Stop", button_type="danger")
stop_button.on_click(stop_action)
send_button = Button(label="Send", button_type="success")
send_button.on_click(send_action)
unit_select = Select(title="Units:", value="step", options=["step", "mm", "micron"])
direction_radio_button_group = RadioButtonGroup(labels=direction_list, active=2)
move_type_radio_buttom_group = RadioButtonGroup(labels=['relative', 'absolute'], active=0)
raw_input = TextInput(value="?AQ", title="Raw Command")
distance_input = TextInput(value="0", title="Distance")
console_div = Div(text="Console")
pre = PreText(text="""Console""", width=WIDTH, height=200)

inputs = widgetbox([title_div, connect_button, direction_radio_button_group, move_type_radio_buttom_group,
                    unit_select, distance_input, move_button, stop_button, raw_input, send_button, console_div,
                    pre], width=WIDTH)

curdoc().add_root(inputs)
curdoc().title = "Newmark Stage Controller"