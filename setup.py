# Always prefer setuptools over distutils
from setuptools import setup

setup(
    name='nmstagepy',
    version='0.1',
    author='Steven Christe',
    author_email='steven.christe@nasa.gov',
    packages=['nmstagepy'],
    url='',
    license='See LICENSE.txt',
    description='Software to control Newmark linear stages controlled by the NSC-M4 controller.',
    long_description=open('README.md').read(),
)
