# -*- coding: utf-8 -*-

import serial
import time
import astropy.units as u
from astropy.units import imperial

import logging

# the time to wait for a response in seconds
TIMEOUT = 0.5
VERBOSE = False


def define_step_unit():
    step = u.def_unit('step', 1/800000. * imperial.inch)
    u.add_enabled_units([step])
    return step


step = define_step_unit()


class Controller(object):

    def __init__(self):
        """

                :param port:
                :raise IOError:
        """

        self.current_axis = 'x'
        self._response_translation = dict(done='!', limit="@", error="#")
        self.ser = None
        # set user unit to one inch, all move commands much be in units of inches
        # self.send_command("UU800000")

    def connect(self, port='COM3'):
        self.ser = serial.Serial(port=port, baudrate=9600, timeout=1)
                                 #bytesize=serial.EIGHTBITS,
                                 #stopbits=serial.STOPBITS_ONE)
        if not self.ser.isOpen:
            raise IOError("Can't connect to Serial port " + port)

    def is_open(self):
        """Check whether the connection is open"""
        return self.ser.isOpen()

    def close(self):
        """Close the connection"""
        self.ser.close()

    def send_command(self, cmd):
        """Send a raw command to the controller"""
        if VERBOSE:
            print("Sending " + cmd)
        out = self.ser.write((cmd + '\n').encode())
        print(out)
        # timeout so that commands are not sent too quickly
        time.sleep(TIMEOUT)
        return out
        #out = ''
        #while self.ser.inWaiting() > 0:
        #    out += self.ser.read(1).decode()
        #return out

    def set_axis(self, axis):
        """Set the axis to control."""
        if axis.lower() == 'x':
            out = self.send_command("AX")
            print(out)
        elif axis.lower() == 'y':
            out = self.send_command("AY")
            print(out)
        elif axis.lower() == "z":
            out = self.send_command("AZ")
            print(out)

        else:
            raise ValueError("Axis not recognized. Must be x,y,z.")

    def get_axis(self):
        """Get which axis is currently set."""
        return self.send_command("?AQ")

    def get_units(self):
        """Report the user units."""
        return self.send_command("?UU")

    def home(self, direction):
        """Find home."""
        if direction.lower() == "forward":
            return self.send_command("HM")
        elif direction.lower() == "reverse":
            return self.send_command("HR")
        else:
            raise ValueError("Direction not recognized. Must be forward or reverse.")

    def get_position(self):
        steps = self.send_command("RP")
        units = self.send_command("RU")
        return steps, units

    def get_position_allax(self):
        return self.send_command("PP")

    @u.quantity_input(n=u.mm)
    def move_absolute(self, n):
        """Move to absolute position n."""
        num_of_steps = int(n.to(step).value)
        print(self.send_command("MA{0}".format(num_of_steps)))
        return self.send_command("GN")

    @u.quantity_input(n=u.mm)
    def move_relative(self, n):
        """Move relative to current position by n."""
        num_of_steps = int(n.to(step).value)
        cmd_str = "MR{0}".format(num_of_steps)
        print(cmd_str)
        print(self.send_command(cmd_str))
        return self.send_command("GN")

    def stop(self):
        """Stop motion along all axis."""
        self.send_command("SA")

    def reset(self):
        """System reset. Current positions along all axes becomes new zero location."""
        self.send_command("RS")


